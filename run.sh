#!/bin/bash

OPENSSL_VERSION="1.1.1d"
OPENSSL_URL="https://www.openssl.org/source/openssl-$OPENSSL_VERSION.tar.gz"
OPENSSL_DIR="openssl-$OPENSSL_VERSION"

LIBEVENT_VERSION="2.1.11-stable"
LIBEVENT_URL="https://github.com/libevent/libevent/releases/download/release-$LIBEVENT_VERSION/libevent-$LIBEVENT_VERSION.tar.gz"
LIBEVENT_DIR="libevent-$LIBEVENT_VERSION"

TOR_GIT_URL="https://git.torproject.org/tor.git"
TOR_DIR="tor"

function download_and_uncompress {
  local output="out.tgz"
  wget -q --show-progress "$1" -O $output
  tar -zxf $output
  rm $output
}

function setup_package {
  if [ -d "$1" ]; then
    echo "[+] Directory $1 found. We'll use that."
    return
  fi

  echo "[+] Directory $1 not found. Downloading from $2"
  download_and_uncompress $2
}

function build_openssl {
  echo "[+] Building OpenSSL..."
  cd $OPENSSL_DIR
  ./Configure --config="../openssl-config.conf" minimal-tor-x86_64
  make -j$(getconf _NPROCESSORS_ONLN)
  cd -
}

function build_libevent {
  echo "[+] Building Libevent..."
  cd $LIBEVENT_DIR
  ./autogen.sh && ./configure --disable-openssl --disable-debug-mode --enable-function-sections
  make -j$(getconf _NPROCESSORS_ONLN)
  cd -
}

function build_tor {
  echo "[+] Building Tor..."
  cd $TOR_DIR
  ./autogen.sh && ./configure --with-openssl-dir="../$OPENSSL_DIR" \
  --enable-static-openssl  --enable-fatal-warnings --enable-static-libevent \
  --with-libevent-dir="../$LIBEVENT_DIR" --disable-module-relay \
  --disable-module-dirauth --disable-asciidoc --disable-unittests \
  --disable-manpage -disable-zstd --disable-lzma --disable-tool-name-check \
  --disable-module-dircache \
  && make -j$(getconf _NPROCESSORS_ONLN)
  cd -
}

# Get OpenSSL sources
setup_package $OPENSSL_DIR $OPENSSL_URL
# Get Libeven sources
setup_package $LIBEVENT_DIR $LIBEVENT_URL

if [ ! -d "$TOR_DIR" ]; then
  echo "[+] Tor directory not found. Cloning from $TOR_GIT_URL"
  git clone $TOR_GIT_URL $TOR_DIR
else
  echo "[+] Directory $TOR_DIR found. We'll use that."
fi

build_openssl

build_libevent

build_tor

strip $TOR_DIR/src/app/tor

echo "[+] Size of tor binary:"
ls -al $TOR_DIR/src/app/tor
